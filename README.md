# Commerce Checkout Accordion

## Introduction

This module provides the accordion checkout experience to the existing commerce
multiple step checkout flow.

Once this module is enabled, it will turn the Order Information Step of the
default Multistep - Default checkout flow into an accordion flow. Any checkout
panes under that step will automatically gain a Next Step button. Clicking the
next step button will validate the checkout pane.

## Summary

- The checkout panes should not be available until the previous step is completed.
Past panes can be clicked (Edit button) to expand but future panes should be
greyed out.
- The Next Step buttons should be doing validation checks on each section before
allowing a customer to continue.
- (@todo) "Continue to Review" should be greyed out until the last pane is openned.

## Requirements

This module currently works with the default Multistep - Default checkout flow.
However, it is in the scope for this module to be more flexible and support more
custom checkout flow.

## Installation

Enable this module just as any other Drupal module.

## Configuration

No configuration is implemented.
